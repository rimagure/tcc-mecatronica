# -*- coding: utf-8 -*-
"""
Created on Sat Nov 11 12:39:22 2017

@author: erich
"""

from watson_developer_cloud import NaturalLanguageUnderstandingV1
from watson_developer_cloud.natural_language_understanding_v1 import Features, EntitiesOptions, KeywordsOptions

import entity_class as ec


def get_time_date(input_text, watson):
    date_entity = None
    time_entity = None

    service = NaturalLanguageUnderstandingV1(
        version='2018-03-16',
        url='https://gateway.watsonplatform.net/natural-language-understanding/api',
        iam_apikey='dt24TGciQ3fiLz8gf9Lv3PZzsyuoItqrZj6wU8TFL4UM')

    response = service.analyze(
        text=input_text,
        features=Features(entities=EntitiesOptions(),
                          keywords=KeywordsOptions())).get_result()

    entities = response['entities']

    mapped_values = {}

    for entity in entities:
        if entity['type'] == 'sys-date':
            mapped_values['date'] = entity['value']
#            locations['date'] = entity['location']
            entity_start = int(entity['location'][0])
            entity_end = int(entity['location'][1])
            date_entity = ec.Entity(input_text[entity_start:entity_end],entity_start,entity_end)
            
        elif entity['type'] == 'sys-time':
            mapped_values['time'] = entity['value']
#            locations['time'] = entity['location']
            entity_start = int(entity['location'][0])
            entity_end = int(entity['location'][1])
            time_entity = ec.Entity(input_text[entity_start:entity_end],entity_start,entity_end)
    
    if not mapped_values:
        return None,None,None
    
    return date_entity,time_entity,mapped_values
#    if not locations:
#        return None, None

#    return entities_recognized, locations